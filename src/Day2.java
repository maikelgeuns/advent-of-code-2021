import java.util.List;

public class Day2 {

    public static void main(String[] args) {
        var inputs = InputFetcher.fetchLocalPuzzleInput(2);
        System.out.println(solveTask1(inputs));
        System.out.println(solveTask2(inputs));


    }

    private static int solveTask1(List<String> inputList) {
        int forwardPosition = 0;
        int depthPosition = 0;
        for (String input: inputList) {
            int position = Integer.parseInt(input.replaceAll("[^0-9]", ""));
            if (input.contains("forward")) {
                forwardPosition = forwardPosition + position;
            } else if (input.contains("up")) {
                depthPosition = depthPosition - position;
            } else if (input.contains("down")) {
                depthPosition = depthPosition + position;
            } else {
                System.out.println(input);
            }
        }
        return forwardPosition * depthPosition;
    }


    private static int solveTask2(List<String> inputList) {
        int forwardPosition = 0;
        int depthPosition = 0;
        int aim = 0;
        for (String input: inputList) {
            int position = Integer.parseInt(input.replaceAll("[^0-9]", ""));
            if (input.contains("forward")) {
                forwardPosition = forwardPosition + position;
                if (aim != 0) {
                    depthPosition = depthPosition + (aim * position);
                }
            } else if (input.contains("up")) {
                aim = aim - position;
            } else if (input.contains("down")) {
                aim = aim + position;
            } else {
                System.out.println(input);
            }
        }
        return forwardPosition * depthPosition;
    }
}
