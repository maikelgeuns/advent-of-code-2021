import java.util.List;

public class Day1 {

    public static void main(String[] args) {

        var inputs = InputFetcher.fetchLocalPuzzleInput(1);
        var inputIntegers = inputs != null ? inputs.stream().map(Integer::parseInt).toList() : null;

        assert inputIntegers != null;
        System.out.println(solveTask1(inputIntegers));
        System.out.println(solveTask2(inputIntegers));


    }

    private static int solveTask1(List<Integer> inputIntegers) {
        int count = 0;
        for (int i = 1; i < inputIntegers.size(); i++) {
            if (inputIntegers.get(i - 1) < inputIntegers.get(i)) {
                count++;
            }
        }

        return count;
    }

    private static int solveTask2(List<Integer> inputIntegers) {
        int count = 0;
        for (int i = 0; i < inputIntegers.size() - 3; i++) {
            if (inputIntegers.get(i) + inputIntegers.get(i + 1) + inputIntegers.get(i + 2)
                    < inputIntegers.get(i + 1) + inputIntegers.get(i + 2) + inputIntegers.get(i + 3) ) {
                count++;
            }
        }

        return count;

    }


}
