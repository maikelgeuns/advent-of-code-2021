import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class InputFetcher {

    public static List<String> fetchLocalPuzzleInput(int dayNumber) {
        try {
            String day = "resources/day" + dayNumber + ".txt";
            return Files.readAllLines(Paths.get(day));
        } catch (IOException ioException) {
            //Do nothing
        }

        return null;
    }
}
